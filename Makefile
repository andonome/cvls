example.pdf: cvls/cv.cls
	$(COMPILER) template.tex

REMOTE != git remote show origin -n | grep Fetch  | tail -c+14
COMPILER = latexmk -file-line-error -auxdir=dross -pdflua -interaction=nonstopmode -halt-on-error -shell-escape -jobname=$(basename $@)

help:
	@echo 'For an example CV, use'
	@echo '$$ make'
	@echo ''
	@echo 'To make your own CV, make a directory,'
	@echo 'then run'
	@echo ''
	@echo '$$ git init'
	@echo '$$ git submodule add $(REMOTE)'
	@echo ''
	@echo '$$ cp cvls/template.tex cv.tex'
	@echo ''

clean:
	$(RM) -r *pdf \
	dross \
	*.md
