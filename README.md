# CVLS: The LaTeX CV Template

For a preview, run

```sh
make example.pdf
```

## Dependencies

- `make`
- `pdflatex`

### LaTeX Packages

- `geometry`
- `etoolbox`
- `tabularx`
- `microtype`
- `xcolor`
- `fontawesome`
- `hyperref`
- `multicol`
- `titlesec`
- `cantarell`

