\ProvidesClass{cvls}

\newif\ifseriff
\serifffalse

\newif\iftwopage
\twopagefalse

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\DeclareOption{seriff}{\serifftrue}
\DeclareOption{twopage}{\twopagetrue}

\ProcessOptions\relax

\LoadClass{article}

\RequirePackage[
	a4paper,
]{geometry}
  \geometry{margin=40pt}

\usepackage[T1]{fontenc}

\RequirePackage{etoolbox}
\RequirePackage{tabularx}
\RequirePackage{microtype}
\RequirePackage{xcolor}
\definecolor{text}{HTML}{2a2a2a}
\definecolor{boxtext}{HTML}{111111}
\definecolor{headings}{HTML}{111112}
\definecolor{subheadings}{HTML}{741112}
\definecolor{shade}{HTML}{F5DD9E}
\definecolor{darkred}{HTML}{892222}
\RequirePackage[
  colorlinks=true,
  allbordercolors={0 0 0},
  urlcolor=darkred,
  pdfborderstyle={/S/U/W 1},
]{hyperref}
\RequirePackage{fontawesome}

\RequirePackage{multicol}
  \setlength\columnsep{4em}
  \def\columnsepruleoclor{\text}

\RequirePackage[nobottomtitles*]{titlesec}

\newif\ifvolunteer

% make a repeat command, to repeat dots.

\newcommand{\Repeat}[1]{%
  \expandafter\@Repeat\expandafter{\the\numexpr #1\relax}%
}

\def\@Repeat#1{%
    \ifnum#1>0
        \expandafter\@@Repeat\expandafter{\the\numexpr #1-1\expandafter\relax\expandafter}%
    \else
        \expandafter\@gobble
    \fi
}
\def\@@Repeat#1#2{%
    \@Repeat{#1}{#2}#2%
}

\newcounter{skillDots}
  \setcounter{skillDots}{0}
\newcounter{emptyDots}
  \setcounter{emptyDots}{0}

\newcommand*\rateSkill[2]{
  \setcounter{emptyDots}{5}
  \addtocounter{emptyDots}{-#1}
  \Repeat{#1}{\faCircle &}
  \Repeat{\value{emptyDots}}{\faCircleO &}
  \Large\tt #2
  \\
}

\newcommand*\freqSkill[2]{%
  \tt
  \ifcase#1\relax%
  \or%
  Familiar
  \or%
  Worked with
  \or%
  Monthly usage
  \or%
  Daily usage
  \else%
  Actively maintaining
  \fi%
  &
  #2
  \\
}

\newcommand{\infoBox}[1]{
  \colorbox{shade}{
    \color{boxtext}
    \begin{tabular}{cl}
      #1
    \end{tabular}
  }
}

\newcommand\website[1]{
      \faFirefox & \href{https://#1}{\textbf{#1}} \\
}

\newcommand\email[1]{
    \faEnvelope &  \href{mailto:#1}{\textbf{#1}} \\
}

\newcommand\gitlab[1]{
    \faGitSquare & \href{https://gitlab.com/#1}{\textbf{Gitlab.com/#1}} \\
}

\newcommand\phone[1]{
    \faPhoneSquare & \textcolor{darkred}{\textbf{#1}} \\
}

\newcommand\linkedin[1]{
    \faLinkedinSquare & \href{https://www.linkedin.com/in/#1}{\textbf{Linkedin.com/in/#1}} \\
}

\newenvironment{skills}{
  \begin{minipage}{\linewidth}
  \section{Skills}
  \renewcommand\arraystretch{1.4}
  \begin{tabular}{@{}c@{}c@{}c@{}c@{}cp{.8\textwidth}}
}{
  \end{tabular}
  \end{minipage}
}

\newcommand*\nameHeader[1]{{\center{\Huge \textbf{#1}}}}

\titleformat
	{\section}
	[block]
	{\color{headings}\scshape\LARGE\raggedright}
	{}
	{0pt}
	{}
	[\color{gray}\titlerule\color{text}]

\newcommand{\entry}[4]{
	{\raggedleft\textsc{#1}\par}
	\expandafter{{\raggedright\large #2}\\}
	\medskip
	\expandafter{{\raggedright\large\textit{\textbf{#3}}}\\[4pt]}
	#4
	\medskip
}

\newcommand*\listentry[3]{
  \textsc{#1} & \textbf{#2} \\
  & {\small #3} \\
}

\ifseriff
\else
  \RequirePackage[default]{cantarell}
\fi

\newcommand\skill[1]{%
  \noindent%
  \textcolor{darkred}{\faExclamationTriangle}~\textbf{Skills:} #1%
}

\newcommand\role[2][ --- \hspace{-1em}]{%
  \vspace{1em}
  \quad \textcolor{subheadings}{#1}\hspace{1em}\textbf{\normalsize#2}
}

\pagenumbering{gobble}
